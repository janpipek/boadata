0.2.6 (2014-11-22)
------------------
* HTML representation for IPython

0.2.5 (2014-11-14)
------------------
* Open files from command line

0.2.4 (2014-10-30)
------------------
* Make less dependent on optional packages

0.2.3 (2014-10-29)
------------------
* Various bug fixes

0.2.2 (2014-10-26)
------------------
* Basic CSV support

0.2.1 (2014-10-23)
------------------
* Fix: required packages

0.2 (2014-10-22)
----------------
* Blinker dependency
* More detailed children add / remove process
* Selectable item list view
