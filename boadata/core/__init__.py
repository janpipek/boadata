'''Classes for data abstraction used in boadata.'''

from data_properties import DataProperties
from data_object import DataObject
from data_node import DataNode, DataTree